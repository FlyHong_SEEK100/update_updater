/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef UPDATE_UI_UPDATER_UI_CONST_H
#define UPDATE_UI_UPDATER_UI_CONST_H

namespace Updater {
constexpr const char *DEFAULT_FONT_FILENAME = "HarmonyOS_Sans_SC_Regular_Small.ttf";
constexpr const char *FB_DEV_PATH = "/dev/graphics/fb0";
constexpr const char *DRM_DEV_PATH = "/dev/dri/card0";

enum class UpdaterMode {
    SDCARD = 0,
    FACTORYRST,
    REBOOTFACTORYRST,
    OTA,
    RECOVER,
    MODEMAX
};
} // namespace Updater
#endif /* UPDATE_UI_HOS_UPDATER_H */
